# Forensic script
Purpose of the script is to gather information from a running Linux or OS X system. Information that can be used in an forensic examination.

**NOTE**: *I haven't been able to test it on OS X yet - if anyone test it, please send me an email with feedback*

User will be asked if she wants to include case data. If so, the user submits it.  
The following information will be gathered and stored in an output file.  
*The output file will be stored where the script is*

- System time, date and timezone
- Hostname
- Public information (IPv4 etc.)
- System IP configuration
- ARP table
- Current internet connections
- Running processes
- Mounted drives and shares
- Users of the system

```python
class Gather():
    def __init__(self, command):
        self.command = command
    def execute(self):
        return subprocess.run(self.command.split(), capture_output=True, text=True)

# Get information about current system time and date, hostname, System IP configuration,
# running processes, mounted drives and shares, and users
HOSTNAME = socket.gethostname()
PUB_IP = urllib.request.urlopen('https://ip.nf/me.txt').read().decode('utf-8')
ARP = Gather('arp -a')
# Variation of command depending on platform
if platform.startswith('linux'): # if Linux
    TODAY = Gather('timedatectl')
    PROCESSES = Gather('ps -ef')
    SYSTEM = Gather('ip a')
    MOUNTS = Gather('mount')
    CONNECTIONS = Gather('ss -ant')
    USERS = Gather('cat /etc/passwd')
elif platform == 'darwin': # if OS X
    TODAY = Gather('timedatectl')
    PROCESSES = Gather('ps -ef')
    SYSTEM = Gather('ifconfig')
    MOUNTS = Gather('diskutil list')
    CONNECTIONS = Gather('netstat -ant')
    USERS = Gather('dscl . list /Users')
elif platform == 'win32': # Windows
    TODAY = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    PROCESSES = Gather('tasklist')
    SYSTEM = Gather('ipconfig /all')
    MOUNTS = Gather('wmic logicaldisk get name, providername, size, deviceid, volumename, description')
    CONNECTIONS = Gather('netstat -ant')
    USERS = Gather('net user')
```

**NOTE**: *The script is tested on Manjaro, Fedora, Raspbian (Debian 11), Kali, and Windows 10 - and needs no dependencies other than what comes with the system*