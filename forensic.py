#!/usr/bin/env python3

from datetime import datetime
import subprocess
import socket
from sys import platform
import urllib.request

__author__          = 'Jonas Ingemarsson'
__copyright__       = 'Copyright 2022'
__version__         = '1.1.3'
__maintainer__      = 'Jonas Ingemarsson'
__email__           = 'ingemarsson.jonas@me.com'
__status__          = 'testing'
__last_change__     = '2022-11-17'
__dependencies__    = 'None'
__about__           = 'Script to gather forensic data from a running Linux, Windows or OS X system'
__tested__          = 'Verified to work on Manjaro, Fedora, Raspbian (Debian 11), Kali, Windows 10'


class Gather():
    def __init__(self, command):
        self.command = command
    def execute(self):
        return subprocess.run(self.command.split(), capture_output=True, text=True)

# Get information about current system time and date, hostname, System IP configuration,
# running processes, mounted drives and shares, and users
HOSTNAME = socket.gethostname()
PUB_IP = urllib.request.urlopen('https://ip.nf/me.txt').read().decode('utf-8')
ARP = Gather('arp -a')
# Variation of command depending on platform
if platform.startswith('linux'): # if Linux
    TODAY = Gather('timedatectl').execute().stdout
    PROCESSES = Gather('ps -ef')
    SYSTEM = Gather('ip a')
    MOUNTS = Gather('mount')
    CONNECTIONS = Gather('ss -ant')
    USERS = Gather('cat /etc/passwd')
elif platform == 'darwin': # if OS X
    TODAY = Gather('date')
    PROCESSES = Gather('ps -ef')
    SYSTEM = Gather('ifconfig')
    MOUNTS = Gather('diskutil list')
    CONNECTIONS = Gather('netstat -ant')
    USERS = Gather('dscl . list /Users')
elif platform == 'win32': # Windows
    TODAY = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    PROCESSES = Gather('tasklist')
    SYSTEM = Gather('ipconfig /all')
    MOUNTS = Gather('wmic logicaldisk get name, providername, size, deviceid, volumename, description')
    CONNECTIONS = Gather('netstat -ant')
    USERS = Gather('net user')


def main():
    # Submit file name of the output
    output = input('[+] Enter name of outputfile: ') or 'output'
    yn = input('[+] Do you want to add case data to outputfile? [Y/n]') or 'y'
    yn.lower()
    output = f'{output}.txt'
    if yn == 'y':
        # In case user wants to add case data to output
        case_no = input('[+] Submit case number: ')
        evidence_no = input('[+] Submit evidence number: ')
        examiner = input('[+] Submit examiner name: ')
        curr_time = input('[+] Submit current date and time [yyyy-mm-dd hh:mm:ss]: ')
        # Writing case data to output file
        with open(output, 'a') as output_file:
            output_file.write(f'{"-" * 10} CASE DATA {"-" * 10}\n')
            output_file.write(f'Case number: {case_no}\n')
            output_file.write(f'Evidence number: {evidence_no}\n')
            output_file.write(f'examiner name: {examiner}\n')
            output_file.write(f'Current time: {curr_time}\n\n')
            output_file.write(f'{"-" * 10} GATHERED DATA BELOW {"-" * 10}\n\n')
    # Writing gathered information to output file
    with open(output, 'a') as output_file:
        output_file.write(f'{"#" * 10} SYSTEM TIME {"#" * 10}\n{TODAY}\n')
        output_file.write(f'\n{"#" * 10} HOSTNAME {"#" * 10}\n{HOSTNAME}\n')
        output_file.write(f'\n{"#" * 10} PUBLIC INFORMATION {"#" * 10}\n{PUB_IP}\n')
        output_file.write(f'\n{"#" * 10} SYSTEM IP CONFIGURATION {"#" * 10}\n{SYSTEM.execute().stdout}')
        output_file.write(f'\n{"#" * 10} ARP TABLE {"#" * 10}\n{ARP.execute().stdout}')
        output_file.write(f'\n{"#" * 10} LIST OF INTERNET CONNECTIONS {"#" * 10}\n{CONNECTIONS.execute().stdout}')
        output_file.write(f'\n{"#" * 10} LIST OF RUNNING PROCESSES {"#" * 10}\n{PROCESSES.execute().stdout}')
        output_file.write(f'\n{"#" * 10} LIST OF MOUNTED DRIVES/SHARES {"#" * 10}\n{MOUNTS.execute().stdout}')
        output_file.write(f'\n{"#" * 10} LIST OF USERS {"#" * 10}\n{USERS.execute().stdout}')
        output_file.write(f'\n\n{"#" * 10} END OF INFORMATION ##########')


if __name__ == '__main__':
    main()
